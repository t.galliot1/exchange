# Exchange



## Installation

- `git clone https://gitlab.com/simplon-roanne/exchange`
- `cd exchange`
- `composer install`
-  Copier .env.example vers .env et remplir les informations DB
- `php artisan migrate`

## Mise à jour du projet

- `php artisan migrate:fresh`